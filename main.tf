resource "random_string" "random" {
  length           = 16
  special          = true
  override_special = "/@£$"
}

resource "local_file" "test" {
  content  = random_string.random.result
  filename = "${path.module}/test.txt"
}
